const fs = require("fs")
const path = require("path");

const pages = fs.readdirSync('./pages')

const dev = true
  
let entry = {
  _polyfil: "./babel.polyfill.min.js"
}

for (let page of pages) {
  entry[page] = `./pages/${page}`
}
 
module.exports = { 
  entry: entry,
  output: { 
    filename: '[name].js',
    path: path.resolve(__dirname, dev? ".build/dev": ".build/prod")
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      }
    ]
  },
  mode: dev? "development": "production"
};
