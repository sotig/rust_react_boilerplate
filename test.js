const fs = require("fs")
const path = require("path");

const pages = fs.readdirSync('./pages')
  
let entry = {}
for (let page of pages) {
  entry[page] = `./pages/${page}`
}
 
let ex = {
  entry: entry,
  output: {
    filename: "bundle.js",
    path: path.resolve(__dirname, "dist")
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      }
    ]
  },
  mode: "development"
};
console.log(ex)